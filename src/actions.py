import os
import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pathlib import Path

import mailparser
from imapclient import IMAPClient, SocketTimeout, imapclient


class IMAPBase:
    connection = {}
    server = None
    ssl_context = None

    DEFAULT_TIMEOUT_CONNECT = 15
    DEFAULT_TIMEOUT_READ = 60

    def connect(self):
        if not self.connection.get("server"):
            raise Exception("No server name or IP provided")

        if not self.connection.get("user"):
            raise Exception("No user name provided")

        if not self.connection.get("pass"):
            raise Exception("No password provided")

        if self.connection.get("skip_ssl_verification", False):
            self.ssl_context = ssl._create_unverified_context()
        else:
            self.ssl_context = ssl.create_default_context()

        self.server = IMAPClient(
            self.connection["server"],
            port=self.connection.get("port", 993),
            ssl_context=self.ssl_context,
            timeout=SocketTimeout(
                connect=self.connection.get("timeout_connect", IMAPBase.DEFAULT_TIMEOUT_CONNECT), 
                read=self.connection.get("timeout_read", IMAPBase.DEFAULT_TIMEOUT_READ)
            )
        )
        self.server.login(self.connection["user"], self.connection["pass"])

    def disconnect(self):
        self.server.logout()

    @staticmethod
    def compile_query(unread=False, sender="", subject="", body="", query=""):
        q = []
        if unread:
            q.append("UNSEEN")
        if sender:
            q.append(f'FROM "{sender}"')
        if subject:
            q.append(f'SUBJECT "{subject}"')
        if body:
            q.append(f'BODY "{body}"')
        if query:
            q.append(query)

        q_string = " ".join(q)
        return q_string


class RetrieveMail(IMAPBase):
    ATT_NO_PROCESSING = "no processing"
    ATT_DOWNLOAD_LOCAL = "download to Worker"
    ATT_STORE_ON_NJINN = "store with Njinn Service"

    unread = True
    sender = ""
    subject = ""
    body = ""
    folder = "INBOX"
    query = ""
    attachments = ATT_NO_PROCESSING
    attachments_path = "."

    attachments_root_folder = "attachments"

    def run(self):
        result = {"mails": []}
        q_string = self.compile_query(
            unread=self.unread,
            sender=self.sender,
            subject=self.subject,
            body=self.body,
            query=self.query,
        )
        
        self.connect()
        with self.server as srv:
            print(f"Retrieving mails from {self.folder} for query: {q_string}")
            message_data_qualifier = b"RFC822"
            srv.select_folder(self.folder)

            mails = srv.search(q_string)
            print(f"Found {len(mails)} matching mails")

            for uid, message_data in srv.fetch(mails, message_data_qualifier).items():
                mail = mailparser.parse_from_bytes(message_data[message_data_qualifier])

                atts = []
                if self.attachments == self.ATT_STORE_ON_NJINN:
                    attachments_path = os.path.join(
                        self.attachments_root_folder, str(uid)
                    )
                    if mail.attachments:
                        if not os.path.exists(attachments_path):
                            os.makedirs(attachments_path)
                        mail.write_attachments(attachments_path)
                        print(f"Downloading attachments for message({uid}).")
                        for at in mail.attachments:
                            print(f"Storing at['filename'].")
                            atts.append(
                                self._njinn.upload_file(
                                    os.path.join(attachments_path, at["filename"])
                                )
                            )
                elif self.attachments == self.ATT_DOWNLOAD_LOCAL:
                    if not os.path.exists(self.attachments_path):
                        os.makedirs(self.attachments_path)
                    print(
                        f"Downloading attachments for message({uid}) to {self.attachments_path}"
                    )
                    mail.write_attachments(self.attachments_path)
                    for at in mail.attachments:
                        print(f"{at['filename']}")
                        atts.append(os.path.join(Path(self.attachments_path), at["filename"]))

                result["mails"].append(
                    {
                        "uid": uid,
                        "from": mail.From,
                        "to": mail.To,
                        "date": mail.date.isoformat(),
                        "headers": mail.headers,
                        "subject": mail.subject,
                        "body": mail.body,
                        "attachments": atts,
                        "defects": mail.defects,
                    }
                )

            return result


class MoveMail(IMAPBase):
    unread = False
    sender = ""
    subject = ""
    body = ""
    query = ""
    source_folder = "INBOX"
    target_folder = ""

    def run(self):
        q_string = self.compile_query(
            unread=self.unread,
            sender=self.sender,
            subject=self.subject,
            body=self.body,
            query=self.query,
        )

        if not self.target_folder:
            raise Exception(
                "Don't know where to move the mail to, no target folder path provided"
            )

        if self.target_folder == self.source_folder:
            print("Source and target folder path are identical, nothing to do")
            return

        self.connect()
        with self.server as srv:
            print(f"Moving mails found by query {q_string}")
            print(f"Source: {self.source_folder}")
            print(f"Target: {self.target_folder}")
            srv.select_folder(self.source_folder)
            mails = srv.search(q_string)

            if len(mails) == 0:
                print("No mails found to move")
                return

            print(f"Found {len(mails)} matching mail(s)")
            print(f"Moving mail(s){mails} ")
            srv.move(imapclient.join_message_ids(mails), self.target_folder)


class SMTPBase:
    connection = {}
    server = None
    ssl_context = None
    start_tls = True

    DEFAULT_TIMEOUT = 15

    def connect(self):
        if not self.connection.get("server"):
            raise Exception("No server name or IP provided")

        if self.connection.get("skip_ssl_verification", False):
            print("Skipping SSL verification")
            self.ssl_context = ssl._create_unverified_context()
        else:
            print("SSL verification turned on")
            self.ssl_context = ssl.create_default_context()

        try:
            mail_server = self.connection["server"]
            port = self.connection.get("port", 465)
            timeout = self.connection.get("timeout", SMTPBase.DEFAULT_TIMEOUT)
            
            print(
                f"Connecting using SSL to {mail_server}:{port} (timeout={timeout}s)... ",
                end="",
            )
            self.server = smtplib.SMTP_SSL(
                mail_server, port=port, context=self.ssl_context, timeout=timeout
            )
            print("successful")
        except:
            print("failed")
            try:
                port = self.connection.get("port", 587)
                print(
                    f"Connecting to {mail_server}:{port} (timeout={timeout}s)... ",
                    end="",
                )
                self.server = smtplib.SMTP(mail_server, port=port, timeout=timeout)
                print("successful")
                print("Upgrading connection via starttls... ", end="")
                self.server.starttls(context=self.ssl_context)
                print("successful")
            except smtplib.SMTPNotSupportedError:
                print("failed")

        if self.connection.get("user"):
            print(f"Attempting to authenticate user {self.connection['user']}")
            self.server.ehlo()
            self.server.login(self.connection["user"], self.connection["pass"])
            print("User successfully authenticated")


class SendMail(SMTPBase):
    sender = ""
    recipients = ""
    cc = ""
    bcc = ""
    subject = ""
    message_plain = ""
    message_html = ""
    attachments = ""

    def run(self):
        message = MIMEMultipart("alternative")
        message["Subject"] = self.subject
        message["From"] = self.sender
        recipients = [r.strip() for r in self.recipients.split(",")]
        message["To"] = ", ".join(recipients)
        if self.cc.strip():
            self.cc = [r.strip() for r in self.cc.split(",")]
            message["Cc"] = ", ".join(self.cc)
            recipients.extend(self.cc)
        if self.bcc.strip():
            self.bcc = [r.strip() for r in self.bcc.split(",")]
            #There is no "Bcc" header. To and Cc headers are for client representation only
            recipients.extend(self.bcc)

        text = self.message_plain
        html = self.message_html

        if self.message_plain:
            message.attach(MIMEText(text, "plain"))

        if self.message_html:
            message.attach(MIMEText(html, "html"))

        if self.attachments:
            lines = self.attachments.splitlines()
            for line in lines:
                line = line.strip()
                # skip empty lines
                if not line:
                    continue
                filepath = Path(line)
                if not filepath.exists():
                    raise Exception(f"{filepath} cannot be found, aborting!")
                if filepath.is_dir():
                    raise Exception(f"{filepath} is a directory, aborting!")
                if filepath.stat().st_size > 10485760:
                    raise Exception(
                        f"{filepath} exceeds attachment size limit of 10MB, aborting!"
                    )
                print(f"Attaching {filepath}")
                with filepath.open(mode="rb") as attachment:
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(attachment.read())

                encoders.encode_base64(part)
                part.add_header(
                    "Content-Disposition",
                    f"attachment; filename= {filepath.name}",
                )
                message.attach(part)

        self.connect()
        print()
        print(f"Sending mail to {recipients}")
        self.server.sendmail(self.sender, recipients, message.as_string())
        print("Done")
